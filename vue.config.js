const path = require("path");

function resolve(dir){
    return path.join(__dirname, dir);
}

module.exports = {
    assetsDir: "asset",
    configureWebpack: config => {
        Object.assign(config.resolve.alias, {
            "@views": resolve("src/views"),
            "@components": resolve("src/components"),
            "@api": resolve("src/api"),
            "@utils": resolve("src/utils"),
            "@libs": resolve("src/libs"),
            "@assets": resolve("src/assets")
        });
    },
    chainWebpack: config => {
        config.plugin("html").tap(args => {
            args[0].VUE_APP_NAME = process.env.VUE_APP_NAME;
            return args;
        });
    },
    devServer: {
        proxy: {
            '/api': {
                target: 'http://s.c.com/',
                changeOrigin: true,
                pathRewrite: { '^/api': '' }
            }
        }
    }
};