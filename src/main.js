import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import schema from "async-validator";
import $scroll from "@utils/loading";
import $dialog from "@utils/dialog";

import "@assets/iconfont/iconfont";
import "@assets/iconfont/iconfont.css";
import "@assets/js/media_750";
import "vue-ydui/dist/ydui.base.css";
import "@assets/css/base.css";
import "@assets/css/reset.css";
import "@assets/css/style.css";

Vue.config.productionTip = false;
Vue.config.devtools = true;
Vue.prototype.$validator = function(rule) {
    return new schema(rule);
};

Vue.prototype.$scroll = $scroll;
Vue.prototype.$dialog = $dialog;
Vue.prototype.HOST = '/api';

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app');
