import Vue from "vue";
import Router from "vue-router";
import module from "./module";
import Index from "@views/home/Index";
import $store from "../store";
//import toLogin from "@libs/login";

Vue.use(Router);

const router = new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "Index",
            meta: {
                title: "首页",
                keepAlive: true,
                footer: false,
                backgroundColor: "#fff"
            },
            component: Index
        },
        ...module
    ]
});
const { back, replace } = router;

router.back = function() {
    this.isBack = true;
    back.call(router);
};
router.replace = function(...args) {
    this.isReplace = true;
    replace.call(router, ...args);
};
router.beforeEach((to, form, next) => {
    const {title, backgroundColor, footer, home, auth} = to.meta;
    if(auth == true && !$store.state.app.token){
        if(form.name == "Login") return;
       // return toLogin(true, to.fullPath);
    }
    document.title = title || process.env.VUE_APP_NAME || "crmeb商城";
    //判断是否显示底部导航
    footer === true ? $store.commit("SHOW_FOOTER") : $store.commit("HIDE_FOOTER");

    //控制悬浮按钮是否显示
    home === false ? $store.commit("HIDE_HOME") : $store.commit("SHOW_HOME");

    $store.commit("BACKGROUND_COLOR", backgroundColor || "#F5F5F5");
    next();
});
export default router;