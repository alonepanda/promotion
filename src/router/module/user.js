import User from "@views/user/User";

export default [
    {
    path: "/user",
    name: "User",
    meta: {
        title: "个人中心",
            keepAlive: true,
            footer: true,
            auth: false
    },
    component: User
    },
    {
        path: "/user/user_promotion",
        name: "UserPromotion",
        meta: {
            title: "我的推广",
            keepAlive: true,
            footer: true,
            auth:false
        },
        component: () => import("@views/user/promotion/UserPromotion.vue")
    },
    {
        path: "/user/poster",
        name: "Poster",
        meta: {
            title: "推广名片",
            keepAlive: false,
            footer: false,
            auth: false
        },
        component: () => import("@views/user/promotion/Poster.vue")
    },
    {
        path: "/user/promoter_list",
        name: "PromoterList",
        meta: {
            title: "推广人统计",
            keepAlive: false,
            footer: false,
            auth: false
        },
        component: () => import("@views/user/promotion/PromoterList.vue")
    },
    {
        path: "/user/commission",
        name: "Commission",
        meta: {
            title: "佣金明细",
            keepAlive: false,
            footer: false,
            auth: false
        },
        component: () => import("@views/user/promotion/Commission.vue")
    },
    {
        path: "/user/promoter_order",
        name: "PromoterOrder",
        meta: {
            title: "推广人订单",
            keepAlive: false,
            footer: false,
            auth: false
        },
        component: () => import("@views/user/promotion/PromoterOrder.vue")
    },
    {
        path: "/user/promotion_orders",
        name: "PromotionOrders",
        meta: {
            title: "推广人订单",
            keepAlive: false,
            footer: false,
            auth: false
        },
         component: () => import("@views/user/promotion/PromotionList.vue")
    },
    {
        path: "/user/cart",
        name: "Cart",
        meta: {
            title: "商品下单",
            keepAlice: false,
            footer: false,
            auth: false
        },
        component: () => import("@views/user/promotion/Cart.vue")
    },
    {
        path: "/user/submit/:id",
        name: "Submit",
        meta: {
            title: "订单页面",
            keepAlive: false,
            footer: false,
            auth: false
        },
        component: () => import("@views/user/promotion/Submit.vue")
    },
    {
        path: "/user/add_address",
        name: "AddAddress",
        meta: {
            title: "新增地址",
            keepAlive: false,
            footer: false,
            auth: false
        },
        component: () => import("@views/user/promotion/AddAddress.vue")
    },
    {
        path: "/user/detail/:id",
        name: "UserDetail",
        meta: {
            title: "订单页面",
            keepAlive: false,
            footer: false,
            auth: false
        },
        component: () => import("@views/user/promotion/DetailOrder.vue")
    },
    {
        path: "/user/orderlist/:type?",
        name: "OrderList",
        meta: {
            title: "订单详情",
            keepAlive: false,
            footer: false,
            auth: false
        },
        component: () => import("@views/user/promotion/OrderList.vue")
    },
    {
        path: "/user/refund/:id",
        name: "UserRefund",
        meta: {
            title: "申请退款",
            keepAlive: false,
            footer: false,
            auth: false
        },
        component: () => import("@views/user/promotion/UserRefund.vue")
    }
];