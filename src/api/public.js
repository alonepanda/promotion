import request from "@utils/request";

export function getWechatConfig(){
    return request.get("/wechat/config", {url: document.location.href}, {login: false});
}

export function wechatAuth(code, spread, login_type){
    return request.get("/wechat/auth", {code, spread, login_type}, {login: false});
}