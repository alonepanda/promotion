import request from "@utils/request";
/**
 * //获取用户信息
 * @returns {*}
 */
export function getUserInfo() {
    return request.get("/userinfo");
}

/**
 * 获取用户资料等
 */
export function getUser() {
    return request.get("/spread/user")
}

/**
 * 获取用户服务菜单
 * @returns {*}
 */
export function getMenuUser() {
    return request.get("/menu/prouser");
}

/**
 * 获取推广信息
 * @returns {*}
 */
export function getSpreadInfo() {
    return request.get("/commission");
}

/**
 * 推广图片获取
 */
export function getSpreadImg() {
    return request.get("/spread/banner");
}

/**
 * 所推广用户统计
 * @param $params
 * @returns {*}
 */
export function getSpreadUser($params) {
    return request.post("/spread/people", $params);
}

/**
 * 佣金明细
 * @param q
 * @param types
 * @returns {*}
 */
export function getCommissionInfo(q, types){
    return request.get("/spread/commission/" + types, q);
}

/**
 * 推广订单
 * @param where
 * @returns {*}
 */
export function getSpreadOrder(where) {
    return request.post("/spread/order", where);
}

export function getProducts(q) {
    return request.get("/spread/products", q);
}

export function getCartList() {
    return request.get("/spread/carts");
}

export function postOrderConfirm(cartId) {
    return request.post("/spread/confirm", { cartId });
}

export function postOrderComputed() {

}

/**
 * 生成订单
 * @param key
 * @param data
 * @returns {*}
 */
export function createOrder(key, data) {
    return request.post("/spread/create/" + key, data);
}

export function getAddressList(data) {
    return request.get("/spread/addressList", data || {});
}

export function getAddress(id) {
    return request.get("", id);
}

export  function postAddress(data) {
    return request.post("/address/edit", data);
}

/**
 * 添加商品预购订单
 * @param data
 * @returns {*}
 */
export function postCartAdd(data) {
    return request.post("/spread/cartAdd", data);
}

/**
 * 查询订单数据
 */
export function getOrderData() {
    return request.get("/spread/orderdata");
}
/**
 * 查询订单列表
 */
export function getOrderList(data) {
    return request.get("/spread/orderlist", data);
}

/**
 * 支付订单
 */
export function payOrder(uni, paytype, from) {
    return request.post("/spread/orderpay", { uni, paytype, from });
}

/**
 * 取消订单
 */
export function cancelOrder(id) {
    return request.post("/spread/cancelorder", {id});
}

/**
 * 订单详情
 */
export function orderDetail(uni) {
    return request.get("/spread/orderdetail", {uni})
}

/**
 * 退货原因
 */
export function getRefundReason() {
    return request.get("/order/refund/reason");
}